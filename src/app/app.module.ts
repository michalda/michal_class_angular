import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user-form/user-form.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';
import {PostsComponent} from './posts/posts.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {FireComponent} from './fire/fire.component';
import{AngularFireModule} from 'angularfire2';

import { UsersService } from './users/users.service';

export const firebaseConfig = {
      apiKey: "AIzaSyByLnJK8mOhfltcIZVGsuV9VzRSr90Yhhg",
    authDomain: "michalangular-62480.firebaseapp.com",
    databaseURL: "https://michalangular-62480.firebaseio.com",
    storageBucket: "michalangular-62480.appspot.com",
    messagingSenderId: "822033829223",
}

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
   { path: 'fire', component: FireComponent },
  { path: '', component: UsersComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserFormComponent,
    PostsComponent,
    UserComponent,
    SpinnerComponent,
    PageNotFoundComponent, 
    FireComponent 
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
