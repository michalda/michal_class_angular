import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {
  private _url = "http://jsonplaceholder.typicode.com/users";
  usersObservable;

  constructor(private af:AngularFire) { }
  
  addUser(user){
    this.usersObservable.push(user);
  }

  deleteUser(user){
      let userkey=user.$key
    this.af.database.object('/users/' + userkey).remove();
    
  }

  updateUser(user){
    let userkey=user.$key
    let userdata = {name:user.name,email:user.email}
    console.log(userdata);
    this.af.database.object('/users/' + userkey).update(userdata)
  }  

  getUsers(){
    this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
	}
}
